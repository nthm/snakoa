# snakoa

A simple Koa based battle snake for the 2018 competition. Originally written
using Express but ported afterwards to help learn the framework.

The game server contacts three snake endpoints all using POST requests:
`/start`, `/move`, and `/end`. This snake only implements the first two.

## `/start`

This is replied to statically with the snake colour, an avatar url, and an
opening taunt (message). The request body is not read or considered.

## `/move`

The snake receives the current game state on each move. See the documentation on
https://battlesnake.io for the specs. The snake is only interested in the
position of food and the snakes currently in play (including itself).

First, two grids are built. One is entirely for us to see in the console to
help debug, called `viewGrid`, and the other is used for determining the next
move, called `dataGrid`.

`viewGrid` draws the board as dots (`.`) everywhere except for food and
snakes. It's not waited for by the rest of the application (it's async with no
await).

`dataGrid` is a grid of integers as well as positive and negative infinity.
Other snakes, including itself, and the borders of the map are marked as
`-Infinity`. Food is marked as `Infinity` and the value of each tile on the
board is increased depending on its distance from the food. The values from
multiple foods stack. Each turn the snake looks at it's 4 non-diagonal
neighbouring tiles and moves in the direction of the highest value.

That's it.

Lots of improvements could be made but this was only a 4 hour competition and
the snake stands up well against a good portion of the competition.
