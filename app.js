const koa = require('koa');
const raw = require('raw-body');
const app = new koa();

const startPayload = {
  head_url: 'http://i0.kym-cdn.com/photos/images/original/000/559/663/102.jpg',
  taunt: 'ohmyohmyohmyohmy',
  color: 'pink',
};

const taunts = [
  'heyo',
  'love me',
  'haha yes',
  'UGH',
  'no step on snek',
  'CRITICAL HIT',
  'um yes',
];

// helpers

const random = array => array[Math.floor(Math.random() * array.length)]

const buildGrid = (w, h, fill) => {
  const grid = new Array(h);
  for (let i = 0; i < h; i++) {
    const row = new Array(w);
    row.fill(fill);
    grid[i] = row;
  }
  return grid;
}

const printGrid = grid => {
  grid.forEach((row, index) => {
    console.log(index + '\t', row.join(' '));
  })
  const columnIndexes = Array.from(Array(grid[0].length).keys());
  console.log('\t', columnIndexes.join(' '));
}

const spread = (grid, pointX, pointY) => {
  for (let x = 0; x < grid.length; x++) {
    for (let y = 0; y < grid[0].length; y++) {
      const distance = Math.abs((pointX - x)) + Math.abs((pointY - y));
      grid[x][y] += parseInt(500 * (1 / distance));
    }
  }
}

const surroundArray = (array, value) => {
  array.unshift(value);
  array.push(value);
}

// include server processing time in the response
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

// snake logic
app.use(async ctx => {
  console.log(`${ctx.method} ${ctx.url} path:`, ctx.path);
  // only allow POSTs
  ctx.assert(ctx.method === 'POST', 405);

  if (ctx.path === '/start') {
    return ctx.body = startPayload;
  }
  // if it's not a /start, it must be /move
  ctx.assert(ctx.path === '/move', 404);

  const rawOptions = {
    length: ctx.req.length,
    encoding: ctx.req.charset || 'utf8',
    limit: '1mb'
  };
  const body = await raw(ctx.req, rawOptions);
  // don't try/catch - just return 500 if this fails
  const json = JSON.parse(body);
  ctx.assert(json && json.you, 204, 'board state not provided');

  // unpack the request into grids
  let [viewGrid, dataGrid] = ['.', 0.0].map(fill => {
    return buildGrid(json.width, json.height, fill);
  });

  printGrid(viewGrid);

  // populate it with snakes
  for (const snake of json.snakes.data) {
    const id = snake.name.charAt(0).toUpperCase();
    for (const p of snake.body.data) {
      // their coordinate system is flipped from ours...
      viewGrid[p.y][p.x] = id;
      dataGrid[p.y][p.x] = -Infinity;
    }
  }
  // with food - and spread across the board
  for (const p of json.food.data) {
    viewGrid[p.y][p.x] = '@';
    // JS is pass-by-reference
    spread(dataGrid, p.y, p.x);
    // spread produces NaN at the food because it is division by zero
    dataGrid[p.y][p.x] = Infinity;
  }

  // debugging
  printGrid(viewGrid);

  // wrap dataGrid in -Infinity to provide a wall
  for (const row of dataGrid) {
    surroundArray(row, -Infinity);
  }
  const infRow = new Array(json.width);
  infRow.fill(-Infinity);
  surroundArray(dataGrid, infRow);

  // the snake head, our current position, is the first element
  const headPos = json.you.body.data[0];

  // surrounding the grid offsets our location. correct it here
  let y = headPos.y + 1;
  let x = headPos.x + 1;

  const neighbours = {
    up:    dataGrid[y - 1][x],
    down:  dataGrid[y + 1][x],
    left:  dataGrid[y][x - 1],
    right: dataGrid[y][x + 1],
  }

  console.log('neighbours of head:', { x, y });
  console.log(neighbours);

  let move;
  let options = 4;
  let maxValue = -Infinity;
  for (const [direction, value] of Object.entries(neighbours)) {
    if (value === -Infinity) {
      options--;
    }
    if (value >= maxValue) {
      maxValue = value;
      move = direction;
    }
  }
  const taunt = options === 0 ? 'x_x' : random(taunts);

  ctx.body = { move, taunt };
  console.log(ctx.body);
});

app.listen(5000);
console.log('listening');
