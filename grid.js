/**
 * For now this only draws a grid with the coordinates and has events for hover
 * and click. It will need a websocket from the Koa server to have updates
 * pushed to it. It's probably best for Koa to pass the entire received payload
 * here and then diff the changes?
 * 
 * Required HTML:
 * 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Grid</title>
  <script src="grid.js"></script>
  <style>
    svg {
      border: 2px solid;
    }
  </style>
</head>
<body>
  <!-- Elements are appended here -->
</body>
</html>
 */

// XXX: Use voko? Would need to be patched for SVGs
document.createSVG = tag =>
  document.createElementNS('http://www.w3.org/2000/svg', tag);

function createSVGBaseGrid({
  squareSize = 50, // In pixels
  width = 10,
  height = 10,
} = {}) {
  const svg = document.createSVG('svg')
  const size = squareSize;
  const pxWidth = width * size;
  const pxHeight = height * size;
  svg.setAttribute('width', pxWidth);
  svg.setAttribute('height', pxHeight);
  svg.setAttribute('viewBox', [0, 0, pxWidth + ' ' + pxHeight]);

  // XXX: This should be a class, that has state, and all that...
  const grid = new Array(height);

  for (let y = 0; y < height; y++) {
    grid[y] = new Array(width);
    for (let x = 0; x < width; x++) {
      const g = document.createSVG('g');
      g.setAttribute('transform', [`translate(${x * size},${y * size})`]);
      
      const box = document.createSVG('rect');
      box.setAttribute('width', size);
      box.setAttribute('height', size);
      box.setAttribute('fill', '#fff');
      box.setAttribute('stroke-width', 1);
      box.setAttribute('stroke', '#ccc');
      box.setAttribute('id', `box-${x}-${y}`); 
      g.appendChild(box);

      const text = document.createSVG('text');
      text.appendChild(document.createTextNode(`(${x},${y})`));
      text.setAttribute('font-size', 10);
      text.setAttribute('font-family', 'monospace');
      text.setAttribute('x', 5);
      text.setAttribute('y', 15);
      g.appendChild(text);

      grid[y][x] = box;
      setupBoxEvents(g);

      svg.appendChild(g);
    }
  }
  return { svg, grid };
}

function setupBoxEvents(group) {
  const box = group.firstChild;
  const text = box.nextElementSibling;
  // Lowkey state management via closure
  let locked = false;
  group.addEventListener('click', event => {
    if (locked) {
      box.setAttribute('fill', '#fff');
      text.setAttribute('fill', '#000');
    } else {
      box.setAttribute('fill', '#000');
      text.setAttribute('fill', '#fff');
    }
    locked = !locked;
  });
  group.addEventListener('mouseover', event => {
    if (locked) {
      return;
    }
    box.setAttribute('fill', '#ddd');
  });
  group.addEventListener('mouseout', event => {
    if (locked) {
      return;
    }
    box.setAttribute('fill', '#fff');
  });
}

document.addEventListener('DOMContentLoaded', () => {
  const { svg, grid } = createSVGBaseGrid();
  document.body.appendChild(svg);
})
