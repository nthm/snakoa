# Koa

The web server for this snake is the latest version of Koa, which underwent a
substantial API redesign when transitioning to v2. Because of this, there's lots
of the documentation and middleware on the internet that is either wrong or
pulls in dependencies that are no longer needed.

When porting the snake from Express to Koa, I wanted to try and understand the
underlying libraries and middleware that are used, and try to see where they can
be updated - many packages delegate work to libraries which have now been
integrated into native Node (like Promises). Sometimes there are still good
reasons for doing this, for instance, Bluebird has been shown to be faster than
the native Promise implementation, but I'd rather to reduce complexity and the
number of dependencies for this project.

## Background

Koa aims to be incredibly minimalist, and doesn't support many typical features
like routing or parsing the body out of the box. It is designed to be as general
purpose as possible - maybe you'll never have routes, or you'll only be working
with streams and parsing a body wouldn't make sense. It's a nice approach.

## Request body parsing

The middleware most starter kits use is `koa-bodyparser`, and while there are
others, they all depend on `co-body` at some point. In Koa v1, `co` was used to
work with generators because `async/await` was not yet fully implemented. This
dependency was removed in v2, but by then many projects and middleware had
depended on parts of `co`. 

Despite the name, `co-body` doesn't depend on `co` (!). It relies on `inflate`,
which inflates a GZipped request, and `raw-body` which collects the body from a
request stream and returns a Promise for when the stream completes. Both are
part of `stream-utils`. Once it has a complete body, it just passes it through
`JSON.parse()`. Nice.

For the competition the requests won't be GZipped, and the chain of dependencies
can be replaced with just:

```js
const raw = require('raw-body')
...
app.use(async (ctx, next) => {
  const rawOptions = {
    length: ctx.req.length,
    encoding: ctx.req.charset || 'utf8',
    limit: '1mb'
  }
  const body = await raw(ctx.req, rawOptions)
  // don't try/catch - just return 500 if this fails
  const json = JSON.parse(body)
  ...
})
```

Which is what is being used in *app.js*

## `await next()` stack

For some reason, if an `assert()` is raised (which calls `.throw()`) then the
stack of pending middleware is ignored, and they are never given the chance to
finish. Couldn't find documentation on this.
